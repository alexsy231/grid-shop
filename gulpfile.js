var gulp = require('gulp'),
    prefixer = require('gulp-autoprefixer'),
    sass = require('gulp-sass'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    concat = require('gulp-concat'),
    cleanCSS = require('gulp-clean-css'),
    imagemin = require('gulp-imagemin');
    browserSync = require('browser-sync');


gulp.task('html', function () {
    gulp.src('./src/index.html')
        .pipe(rigger())
        .pipe(gulp.dest('build/'));
});

gulp.task('browserSync', function () {
    browserSync({
        server: {
            baseDir: './build/'
        }
    })
});

gulp.task('sass', function () {
    gulp.src('./src/styles/main.scss')
        .pipe(sass({
            outputStyle: 'compressed',
            includePaths: require('node-normalize-scss').includePaths
        }))
        .pipe(concat('main.css'))
        .pipe(sass())
        .pipe(prefixer())
        .pipe(cleanCSS())
        .pipe(cssmin())
        .pipe(gulp.dest('build/styles'));
});

gulp.task('fonts', function () {
    gulp.src('./src/fonts/*')
        .pipe(gulp.dest('build/fonts/'));
});

gulp.task('img', function () {
        gulp.src('./src/img/**/*.*')
        .pipe(imagemin({
            progressive: true,
            optimizationLevel: 8,
            svgoPlugins: [{
                removeViewBox: false
            }],
            interlaced: true
        }))
        .pipe(gulp.dest('build/img'));
});

gulp.task('watch', function () {
    gulp.watch('src/style/**/*.scss', ['sass']);
    gulp.watch('src/templates/**/*.html', ['html']);

});

gulp.task('build', ['sass', 'html', 'fonts', 'img']);